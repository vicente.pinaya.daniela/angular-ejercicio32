import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  usuario:any;
  constructor(private servioUsuario:UsuarioService) { }

  ngOnInit(): void {
    this.servioUsuario.obtenerUsuario().subscribe({
      next:user => {
        // console.log(this.usuario);
        this.usuario = user['results'][0];
      },
      error:error => {
        console.log(error);
      },

      complete:()=>{
        console.log('mision cumplida'); 
      }
     
    })
  }
}
