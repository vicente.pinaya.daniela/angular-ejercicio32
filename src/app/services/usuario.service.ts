import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(public http: HttpClient) { }

  url= 'https://reqres.in'

  obtenerUsuario(): Observable<any>{
    return this.http.get<any>(this.url);
  }
  
}
